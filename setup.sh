#/bin/bash

# Simple script for deploying project on server.


if [ ! -z $ENVPARAMS ]; then
    echo "Environment Parameters missing."
    exit 1
fi

if [ ! -z "$INSTALL_DIR" ]; then
    echo "Installation directory not specified."
    exit 1
fi

if [ ! -z $PASSKEY ]; then
    echo "Passkey not provided."
    exit 1
fi

if [ -f "docker-composer.yml" ]; then
    echo "shuttng down services"
    cd $INSTALL_DIR
    docker-compose down

    if [[ "$?" = 1 ]]; then
        echo "Error occured while shutting down containers"
        exit 1
    fi

    echo "removing existing folder"
    cd $HOME
    rm -rf $INSTALL_DIR/*
    mkdir "$HOME/$INSTALL_DIR"

    cd $INSTALL_DIR
    git clone git@bitbucket.org:ndy40/rss_feed.git .
fi

echo "Current Directory: $PWD"

# Decrypt the ENVPARAMS into a .env file before starting up docker.
echo "Generating parameter files:"

echo $ENVPARAMS | openssl enc -d -aes-256-cbc -a -out .env  -k "$PASSKEY"

if [ ! -d ".env" ]; then
    echo "Error generating parameter file."
    exit;
fi

source .env

docker-compose up -d





