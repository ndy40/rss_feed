from fabric import task
import os

# connection = Connection(host=os.environ['DEPLOY_SERVER'], user=os.environ['DEPLOY_USER'])
install_dir = os.environ['INSTALL_DIR']
required_files = ['docker-compose.yml', '.env']
config_fields = ['PORT','DOMAIN','RUN_MIGRATIONS','CREATE_ADMIN','ADMIN_USERNAME','ADMIN_PASSWORD','DATABASE_URL','DB_USER','DB_PASS','INSTALL_DIR']

@task
def build(c):
    with open('.env', 'w+') as f:
        for i in config_fields:
            if i in os.environ:
                print("new fields {} val {}".format(i, os.environ[i]))
                if i in os.environ:
                    f.write("{}={}\r\n".format(i, os.environ.get(i)))

@task
def deploy(c):
    with c.sftp() as client:
        if install_dir not in client.listdir():
            # create the missing directory.
            client.mkdir(install_dir)
            #  create the env file and upload to server.

        client.chdir(install_dir)
        for file in required_files:
            print(client.put(file, file, confirm=True))

        print(client.listdir())

    print(c.run(f'cd {install_dir}'))
    print(c.run('ls -al'))
    # c.run('docker-compose down')
    # print(c.run('docker-composer up --build -d'))








